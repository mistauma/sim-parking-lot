const randomLetter = require('random-letters')
const randomSentence = require('random-sentence')

export default (context, inject) => {
  const random = {
    license: () => { return license() },
    name: () => { return toTitleCase(randomSentence({ min: 2, max: 4 }).slice(0, -1)) }
  }
  inject('random', random)
}

const license = () => {
  let license = randomLetter(2).toUpperCase() + ' ' + Math.round(Math.random() * 9999);
  if (Math.random() > 0.5) {
    license += ' ' + randomLetter(2).toUpperCase();
  } else {
    license += ' ' + randomLetter(3).toUpperCase();
  }
  return license;
}

const toTitleCase = (str) => {
  return str.replace(
    /\w\S*/g,
    function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}
