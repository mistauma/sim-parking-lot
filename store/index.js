export const state = () => ({
  filter: false,
  title: 'Parking Lots'
})

export const actions = {
  setFilter ({ commit }, filter) {
    commit('setFilter', filter)
  },

  setTitle ({ commit }, title) {
    commit('setTitle', title)
  },

  toggleFilter ({ commit }) {
    commit('toggleFilter')
  },
}

export const mutations = {
  setFilter (state, filter) {
    state.filter = filter
  },

  setTitle (state, title) {
    state.title = title
  },

  toggleFilter (state) {
    state.filter = !state.filter
  },
}